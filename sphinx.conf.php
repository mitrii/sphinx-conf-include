#!/usr/bin/php

<?php

// config

$configs = array(
    'name' => array(
        'config' => '/path/to/file.conf',
        'params' => '/path/to/file.params.php',
    ),    
);

define('CONFIGS_DIR', './conf.d');


// code

chdir(dirname(__FILE__));

if (file_exists(CONFIGS_DIR))
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator(CONFIGS_DIR)) as $filename) {
        $configs[basename($filename)]['config'] = $filename;
    }

foreach ($configs as $name => $value) {
    

    if (file_exists($value['config'])) {
        $config = file_get_contents($value['config']);
        
        $params_file = (isset($value['params'])) ? $value['params'] : false;
        if ($params_file !== false && file_exists($params_file)) {
            $params = require($params_file);
            $config = str_replace(array_keys($params), array_values($params), $config);
        }


        echo PHP_EOL . PHP_EOL . "### {$value['config']} ###" . PHP_EOL . PHP_EOL;
        echo $config;
        echo PHP_EOL;
    }
}